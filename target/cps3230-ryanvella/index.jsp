<html>
<body>
<h2>Payment Processing Form</h2>
<form name="processForm" action="display.jsp" method="post">
    <table border="0">
        <tbody>
        <tr>
            <td>Name: </td>
            <td><input type="text" name="name" value="" size="50" ></td>
        </tr>
        <tr>
            <td>Address: </td>
            <td><input type="text" name="address" value="" size="50" ></td>
        </tr>
        <tr>
            <td>Card Type: </td>
            <td><select name="type">
                <option>VISA</option>
                <option>Mastercard</option>
                <option>American Express</option>
            </select></td>
        </tr>
        <tr>
            <td>Card Number: </td>
            <td><input type="number" name="cardNum" value="" size="16" ></td>
        </tr>
        <tr>
            <td>Expiry Date: </td>
            <td><input type="text" name="expiry" value="MM/YY" size="4" ></td>
        </tr>
        <tr>
            <td>CVV Code: </td>
            <td><input type="number" name="cvv" value="" size="3" ></td>
        </tr>
        <tr>
            <td>Amount(&#8364;): </td>
            <td><input type="number" name="amount" value="" size="50" ></td>
        </tr>
        </tbody>
    </table>
    <input type="reset" value="Clear" name="clear"/>
    <input type="submit" value="Submit" name="submit"/>
</form>
</body>
</html>
