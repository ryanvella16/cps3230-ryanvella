<%@ page import="com.cps3230.PaymentProcessor" %>
<%@ page import="com.cps3230.BankProxy.BankProxy" %>
<%@ page import="com.cps3230.CCInfo" %>
<%@ page import="static org.mockito.Mockito.mock" %>
<%--
  Created by IntelliJ IDEA.
  User: ryanv
  Date: 05/01/2019
  Time: 11:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Display</title>
</head>
<body>
    <%
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String type = request.getParameter("type");
        String cardNum = request.getParameter("cardNum");
        String expiry = request.getParameter("expiry");
        String cvv = request.getParameter("cvv");
        String amount = request.getParameter("amount");
        long num = Long.parseLong(amount);

        CCInfo newCard = new CCInfo(name,address,type,cardNum,expiry,cvv);
        BankProxy bp = mock(BankProxy.class);
        PaymentProcessor paymentProcessor = new PaymentProcessor(BankProxy bp);


        int flag = 0;
        try {
            flag = paymentProcessor.processPayment(newCard, num);
        } catch (Exception e) {
            e.printStackTrace();
        }
    %>

    <h2>Return Code - <%= paymentProcessor.processPayment(newCard,123) %></h2>
    <h2>Flag - <%= flag %></h2>

</body>
</html>
