package com.cps3230;

import com.cps3230.BankProxy.BankProxy;
import com.cps3230.Transaction;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PaymentProcessor {

   BankProxy bp;

    public PaymentProcessor(BankProxy bp) {
        this.bp = bp;
    }

    public int processPayment(CCInfo card, long amount) throws Exception {
        int code = verifyOffline(card);

        if (code == 0) {
            code = processAuth(card, amount);
        }
        return code;


    }

    public int processAuth (CCInfo card, long amount) throws Exception {

        long cardAuthorisation = bp.auth(card, amount);

        if (cardAuthorisation >= 0) {
            Transaction transaction = new Transaction(cardAuthorisation, amount, "Authorised", card);
            System.out.println("Authorized");
            return (int) cardAuthorisation;
        } else if (cardAuthorisation == -1) {
            System.out.println("credit card details invalid");
            return -1;
        } else if (cardAuthorisation == -2) {
            System.out.println("insufficient funds");
            return -2;
        } else
            return -3;
    }

    public int processCapture (long id) throws Exception {
        long cardCapture = bp.capture(id);
        return 0;
    }


    public int verifyCardType (String cardNo, String cardType) throws Exception {
        int length = cardNo.length();
        String prefix = cardNo.substring(0, 2);
        String msg = "";
        int checkIfValid = 0;

        switch (length) {
            case 13:
                    if (prefix.charAt(0) == '4' && cardType == "VISA") {
                    checkIfValid = 1;
                }
                if (cardType != "VISA"){
                    checkIfValid = -1;
                }
                break;
            case 15:
                if (prefix == "34" || prefix == "37" && cardType == "American Express") {
                    msg = "American Express";
                    checkIfValid = 1;
                }
                if (cardType != "American Express"){
                    checkIfValid = -1;
                }
                break;
            case 16:
                if (cardNo.substring(0, 2) == "51" || cardNo.substring(0, 2) == "52" || cardNo.substring(0, 2) == "53" || cardNo.substring(0, 2) == "54" || cardNo.substring(0, 2) == "55" && cardType == "Mastercard") {
                    msg = "Mastercard";
                    checkIfValid = 1;
                }
                else if (prefix.charAt(0) == '4' && cardType == "VISA") {
                    msg = "VISA";
                    checkIfValid = 1;
                } else {
                    checkIfValid = -1;
                }
                break;
            default:
                msg = "Invalid";
                checkIfValid = -1;
                break;
        }

        return checkIfValid;

    }

    public boolean verifyExpiryDate (String cardDate) throws  Exception {
        //check for valid expiry date
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yy");
        simpleDateFormat.setLenient(false);
        Date expiry = simpleDateFormat.parse(cardDate);
        boolean invalidExpiryDate = expiry.before(new Date());

        if (invalidExpiryDate) {
           return false;
        } else {
            return true;
        }
    }

    public boolean verifyMissingInfo (CCInfo card) throws Exception {
        boolean missingInformation = false;
        if (card.cardCVV == "" || card.customerAddress== "" || card.customerName == "" || card.cardNumber == "" || card.cardExpiryDate == "" || card.cardType == ""){
            missingInformation = true;
        }
        if (missingInformation)
            return true;
        else
            return false;
    }



    public int verifyOffline (CCInfo card) throws Exception {
        int code = 0;
        if (verifyMissingInfo(card)) {
            code = -3;
        }
        else if (verifyLuhn(card.cardNumber) && verifyExpiryDate(card.cardExpiryDate)) {
            if (verifyCardType(card.cardNumber, card.cardType) == 1){
                code = 0;
            } else {
                code = -4;
            }
        } else if (!verifyLuhn(card.cardNumber)) {
            code = -1;
        } else if (!verifyExpiryDate(card.cardExpiryDate)) {
            code = -2;
        } else {
            code = -4;
        }
        return code;
    }

    public boolean verifyLuhn(String cardNumber) {
        int[] cardNumberArray = new int[cardNumber.length()];

        for(int i = 0; i < cardNumber.length(); i++){
            cardNumberArray[i] = Integer.parseInt(cardNumber.substring(i, i+1));
        }

        for (int i = cardNumberArray.length - 2; i >= 0; i = i - 2){
            int j = cardNumberArray[i];
            j = j*2;

            if (j>9){
                j = j % 10 + 1;
            }
            cardNumberArray [i] = j;
        }

        int sum = 0;

        for (int i = 0; i < cardNumberArray.length; i++){
            sum += cardNumberArray[i];
        }

        if (sum % 10 == 0) {
            return true;
        } else
            return false;
    }
}
