package com.cps3230.task1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TransactionDatabase {

    //this property indicates that the value stored has changed.
    boolean isDirty = true;

    Map<Long, Transaction> db;

    public TransactionDatabase() {

        db = new HashMap<Long, Transaction>();
    }

    public void saveTransaction(Transaction t) throws Exception {
        if (t == null){
            throw new Exception("Transaction cannot be null");
        }

        db.put(t.id, t);

        isDirty = true;
    }

    public void removeTransaction (long id) {
        db.remove(id);
        isDirty = true;
    }

    public int totalTransactions(){

        return db.size();
    }

    public boolean commit(DBConnection dbConnection) {
        if(isDirty) {
            Iterator<Transaction> allTransactions = db.values().iterator();

            while (allTransactions.hasNext()) {
                int result = dbConnection.commitTransaction(allTransactions.next());

                if (result != 0) {
                    return false;
                }
            }

            isDirty = false;
        }

        return true;
    }

}
