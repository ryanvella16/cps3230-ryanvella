package com.cps3230.task1;

public class CCInfo {
    public String customerName;
    public String customerAddress;
    public String cardType;
    public String cardNumber;
    public String cardExpiryDate;
    public String cardCVV;

    public CCInfo (String customerName, String customerAddress, String cardType, String cardNumber, String cardExpiryDate, String cardCVV) {
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.cardExpiryDate = cardExpiryDate;
        this.cardCVV = cardCVV;
    }
}
