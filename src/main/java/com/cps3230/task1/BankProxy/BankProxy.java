package com.cps3230.task1.BankProxy;

import com.cps3230.task1.CCInfo;

public interface BankProxy {
    long auth (CCInfo card, long amount);
    int capture (long transactionId);
    int refund (long transactionId, long amount);

}
