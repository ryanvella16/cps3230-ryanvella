package com.cps3230.task1;

public class Transaction {
    public long id;
    public long amount;
    public String state;
    public CCInfo info;


    public Transaction(long id, long amount, String state, CCInfo info) {
        this.id = id;
        this.amount = amount;
        this.state = state;
        this.info = info;
    }
}
