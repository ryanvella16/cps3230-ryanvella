package com.cps3230.task1;

public interface DBConnection {
    int commitTransaction(Transaction t);
}
