package com.cps3230.task1;

import com.cps3230.task1.BankProxy.BankProxy;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PaymentProcessor {

   BankProxy bp;

    public PaymentProcessor(BankProxy bp) {

        this.bp = bp;
    }

    public int processPayment(CCInfo card, long amount) throws Exception {
        int codeVO = verifyOffline(card);
        int codePP;
        if (codeVO == 0)
            codePP = processAuth(card, amount);
        else if (codeVO == -1 || codeVO == -2)
            codePP = 1;
        else
            codePP = 2;

        if (codePP == 1 || codePP == 2)
            codePP = 1;
        else if (codeVO == -4)
            codePP = 2;
        else
            codePP = 0;

        return codePP;
    }

    public int verifyOffline (CCInfo card) throws Exception {
        int code;
        if (verifyMissingInfo(card)) {
            code = -3;
        }
        else if (verifyLuhn(card.cardNumber) && verifyExpiryDate(card.cardExpiryDate)) {
            if (verifyCardType(card.cardNumber, card.cardType) == 1){
                code = 0;
            } else {
                code = -4;
            }
        } else if (!verifyLuhn(card.cardNumber)) {
            code = -1;
        } else if (!verifyExpiryDate(card.cardExpiryDate)) {
            code = -2;
        } else {
            code = -4;
        }
        return code;
    }

    public int processAuth (CCInfo card, long amount) throws Exception {

        long cardAuthorisation = bp.auth(card, amount);

        if (cardAuthorisation >= 0) {
            Transaction transaction = new Transaction(cardAuthorisation, amount, "Authorised", card);
            System.out.println("Authorized");
            return (int) cardAuthorisation;
        } else if (cardAuthorisation == -1) {
            System.out.println("credit card details invalid");
            return -1;
        } else if (cardAuthorisation == -2) {
            System.out.println("insufficient funds");
            return -2;
        } else
            System.out.println("Unknown Error");
            return -3;
    }

    public int processCapture (long id) throws Exception {
        long cardCapture = bp.capture(id);

        if (cardCapture >= 0) {
            System.out.println("Transaction Captured");
            return (int) cardCapture;
        } else if (cardCapture == -1){
            System.out.println("Transaction does not exist");
            return -1;
        } else if (cardCapture == -2) {
            System.out.println("Transaction already has been captured");
            return -2;
        } else if (cardCapture == -3) {
            System.out.println("More than 7 days has passed. Unable to process");
            return -3;
        } else {
            System.out.println("Unknown Error");
            return -4;
        }
    }

    public int processRefund (long id, long amount) throws Exception {
        long cardRefund = bp.refund(id, amount);

        if (cardRefund >= 0) {
            System.out.println("Transaction Refunded");
            return (int) cardRefund;
        } else if (cardRefund == -1){
            System.out.println("Transaction does not exist");
            return -1;
        } else if (cardRefund == -2) {
            System.out.println("Transaction has not been captured");
            return -2;
        } else if (cardRefund == -3) {
            System.out.println("Refund has already been processed");
            return -3;
        } else if (cardRefund == -4){
            System.out.println("Refund amount is greater than captured amount");
            return -4;
        } else {
            System.out.println("Unknown Error");
            return -5;
        }
    }


    public int verifyCardType (String cardNo, String cardType) throws Exception {
        int length = cardNo.length();
        String prefix = cardNo.substring(0, 2);
        int checkIfValid = 0;

        switch (length) {
            case 13:
                    if (prefix.charAt(0) == '4' && cardType.equals("VISA")) {
                    checkIfValid = 1;
                }
                if (!cardType.equals("VISA")){
                    checkIfValid = -1;
                }
                break;
            case 15:
                if (prefix.equals("34") || prefix.equals("37") && cardType.equals("American Express")) {
                    checkIfValid = 1;
                }
                if (!cardType.equals("American Express")){
                    checkIfValid = -1;
                }
                break;
            case 16:
                if (cardNo.substring(0, 2).equals("51") || cardNo.substring(0, 2).equals("52") || cardNo.substring(0, 2).equals("53")  || cardNo.substring(0, 2).equals("54") || cardNo.substring(0, 2).equals("55") && !cardType.equals("Mastercard")) {
                    checkIfValid = 1;
                }
                else if (prefix.charAt(0) == '4' && cardType.equals("VISA")) {
                    checkIfValid = 1;
                } else {
                    checkIfValid = -1;
                }
                break;
            default:
                checkIfValid = -1;
                break;
        }

        return checkIfValid;

    }

    public boolean verifyExpiryDate (String cardDate) throws  Exception {
        //check for valid expiry date
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yy");
        simpleDateFormat.setLenient(false);
        Date expiry = simpleDateFormat.parse(cardDate);
        boolean invalidExpiryDate = expiry.before(new Date());

        if (invalidExpiryDate) {
           return false;
        } else {
            return true;
        }
    }

    public boolean verifyMissingInfo (CCInfo card) throws Exception {
        boolean missingInformation = false;
        if (card.cardCVV.equals("") || card.customerAddress.equals("") || card.customerName.equals("") || card.cardNumber.equals("") || card.cardExpiryDate.equals("") || card.cardType.equals("")){
            missingInformation = true;
        }
        if (missingInformation)
            return true;
        else
            return false;
    }





    public boolean verifyLuhn(String cardNumber) {
        int[] cardNumberArray = new int[cardNumber.length()];

        for(int i = 0; i < cardNumber.length(); i++){
            cardNumberArray[i] = Integer.parseInt(cardNumber.substring(i, i+1));
        }

        for (int i = cardNumberArray.length - 2; i >= 0; i = i - 2){
            int j = cardNumberArray[i];
            j = j*2;

            if (j>9){
                j = j % 10 + 1;
            }
            cardNumberArray [i] = j;
        }

        int sum = 0;

        for (int i = 0; i < cardNumberArray.length; i++){
            sum += cardNumberArray[i];
        }

        if (sum % 10 == 0) {
            return true;
        } else
            return false;
    }
}
