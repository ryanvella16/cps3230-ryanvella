package com.cps3230.BankProxy;

import com.cps3230.CCInfo;

public interface BankProxy {
    public long auth (CCInfo card, long amount);
    public int capture (long transactionId);
    public int refund (long transactionId, long amount);

}
