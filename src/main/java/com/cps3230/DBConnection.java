package com.cps3230;

public interface DBConnection {
    int commitTransaction(Transaction t);
}
