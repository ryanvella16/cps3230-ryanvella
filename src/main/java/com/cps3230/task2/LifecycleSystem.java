package com.cps3230.task2;

public class LifecycleSystem {
    private boolean notVerified = true, verified = false, authorised = false, captured = false, voided = false, refund = false;

    public boolean isVerified(){
        return verified;
    }
    public boolean isAuthorised(){

        return authorised;
    }

    public boolean isCaptured(){

        return captured;
    }

    public boolean isVoided(){

        return voided;
    }

    public boolean isRefund(){

        return refund;
    }

    public void transactionVerified(){
        if(notVerified) {
            verified = true;
        }
    }

    public void authoriseTransaction() {
        if (verified && !authorised) {
            authorised = true;
        }
    }

    public void captureTransaction(){
        if(verified && authorised && !captured) {
            captured = true;
        }
    }

   public void refundTransaction(){
            if (verified && authorised && captured && !refund) {
                refund = true;
            }
        }

   public void voidTransaction() {
            if (verified && authorised && !voided){
                voided = true;
            }
        }

}
