import com.cps3230.enums.TransactionStates;
import com.cps3230.task2.LifecycleSystem;
import nz.ac.waikato.modeljunit.Action;
import nz.ac.waikato.modeljunit.FsmModel;
import nz.ac.waikato.modeljunit.GreedyTester;
import nz.ac.waikato.modeljunit.StopOnFailureListener;
import nz.ac.waikato.modeljunit.Tester;
import nz.ac.waikato.modeljunit.coverage.ActionCoverage;
import nz.ac.waikato.modeljunit.coverage.StateCoverage;
import nz.ac.waikato.modeljunit.coverage.TransitionPairCoverage;
import org.junit.Test;
import nz.ac.waikato.modeljunit.*;
import java.util.Random;
import static org.junit.Assert.assertEquals;

public class TransactionModelTests implements FsmModel {
    private LifecycleSystem systemTest;
    private TransactionStates transactionStates;
    private boolean authorised, captured, voided, refund, verified;

    public TransactionStates getState() {
        return transactionStates;
    }

    public void reset(final boolean var) {
        transactionStates = TransactionStates.NOT_VERIFIED;
        authorised = false;
        captured = false;
        voided = false;
        refund = false;
        verified = false;

        if (var) {
            systemTest = new LifecycleSystem();
        }

    }

    public boolean verifyTransactionGuard() {
        return getState().equals(TransactionStates.NOT_VERIFIED);
    }

    public @Action void verifyTransaction(){
        //UPDATE SUT
        systemTest.transactionVerified();

        verified = true;
        transactionStates = TransactionStates.VERIFIED;
        //Check Correspondence between model and system under test
        assertEquals("The model's verified state doesn't match the SUT's state.", verified, systemTest.isVerified());
    }

    public boolean authoriseTransactionGuard() {
        return getState().equals(TransactionStates.VERIFIED);
    }
    public @Action void authoriseTransaction() {
        //Update SUT
        systemTest.authoriseTransaction();

        //Update Model
        authorised = true;
        transactionStates = TransactionStates.AUTHORISED;

        //Check Correspondence between model and system under test
        assertEquals("The model's authorised state doesn't match the SUT's state.", authorised, systemTest.isAuthorised());
    }

    public boolean voidedTransactionGuard() {
        return getState().equals(TransactionStates.AUTHORISED);
    }
    public @Action void voidedTransaction() {
        //Update SUT
        systemTest.voidTransaction();

        //Update Model
        voided = true;
        transactionStates = TransactionStates.VOID;

        //Check Correspondence between model and system under test
        assertEquals("The model's void state doesn't match the SUT's state.", voided, systemTest.isVoided());
    }

    public boolean capturedTransactionGuard() {
        return getState().equals(TransactionStates.AUTHORISED);
    }
    public @Action void capturedTransaction() {
        //Update SUT
        systemTest.captureTransaction();

        //Update Model
        captured = true;
        transactionStates = TransactionStates.CAPTURED;

        //Check Correspondence between model and system under test
        assertEquals("The model's captured state doesn't match the SUT's state.", captured, systemTest.isCaptured());
    }

    public boolean refundedTransactionGuard() {
        return getState().equals(TransactionStates.CAPTURED);
    }
    public @Action void refundedTransaction() {
        //Update SUT
        systemTest.refundTransaction();
        //Update Model
        refund = true;
        transactionStates = TransactionStates.REFUND;

        //Check Correspondence between model and system under test
        assertEquals("The model's captured state doesn't match the SUT's state.", refund, systemTest.isRefund());
    }

    @Test
    public void main(){
        Tester tester = new GreedyTester(new TransactionModelTests());
        tester.setRandom(new Random());
        final GraphListener graphListener = tester.buildGraph();
        tester.addListener(new StopOnFailureListener());
        tester.addListener("verbose");
        tester.addCoverageMetric(new TransitionPairCoverage());
        tester.addCoverageMetric(new StateCoverage());
        tester.addCoverageMetric(new ActionCoverage());
        tester.generate(500);
        tester.printCoverage();
    }
}
