package com.cps3230.stubs;

import com.cps3230.task1.DBConnection;
import com.cps3230.task1.Transaction;

public class DBConnectionSuccessfulSpy implements DBConnection {

    int numberOfTimesCalled = 0;

    public int commitTransaction(Transaction t) {
        numberOfTimesCalled++;
        return 0;
    }

    public int getNumberOfTimesCalled() {
        return numberOfTimesCalled;
    }

    public void reset() {
        numberOfTimesCalled = 0;
    }
}
