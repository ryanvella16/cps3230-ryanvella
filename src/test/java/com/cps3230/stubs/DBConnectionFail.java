package com.cps3230.stubs;

import com.cps3230.task1.DBConnection;
import com.cps3230.task1.Transaction;

public class DBConnectionFail implements DBConnection {
    public int commitTransaction (Transaction t) {
        return -1;
    }
}
