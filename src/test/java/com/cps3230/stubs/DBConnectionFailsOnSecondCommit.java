package com.cps3230.stubs;

import com.cps3230.task1.DBConnection;
import com.cps3230.task1.Transaction;

public class DBConnectionFailsOnSecondCommit implements DBConnection {
    int numOfCalls = 0;

    public int commitTransaction(Transaction t) {
        numOfCalls++;
        if (numOfCalls == 2) {
            return -1;
        }
        return 0;
    }

}
