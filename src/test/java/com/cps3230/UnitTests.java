package com.cps3230;

import com.cps3230.task1.*;
import com.cps3230.task1.BankProxy.BankProxy;
import com.cps3230.stubs.DBConnectionFail;
import com.cps3230.stubs.DBConnectionFailsOnSecondCommit;
import com.cps3230.stubs.DBConnectionSuccessful;
import com.cps3230.stubs.DBConnectionSuccessfulSpy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.Null;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class UnitTests {
    PaymentProcessor pp ;
    BankProxy bp = mock(BankProxy.class);
    TransactionDatabase db;

    @Before
    public void setup() {
        pp = new PaymentProcessor(bp);
        db = new TransactionDatabase();
    }

    @After
    public void teardown() {
        pp = null;
        db = null;
    }

    @Test
    public void testVerifyLuhnCorrectVisa16Digits() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");


        // Exercise
        boolean result = pp.verifyLuhn(card.cardNumber);

        // Verify
        assertTrue(result);
    }

    @Test
    public void testVerifyLuhnCorrectVisa13Digits() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4806699456584", "12/19", "231");


        // Exercise
        boolean result = pp.verifyLuhn(card.cardNumber);

        // Verify
        assertTrue(result);
    }

    @Test
    public void testVerifyLuhnCorrectMastercard() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Mastercard", "5137419418929912", "12/19", "231");


        // Exercise
        boolean result = pp.verifyLuhn(card.cardNumber);

        // Verify
        assertTrue(result);
    }

    @Test
    public void testVerifyLuhnCorrectAmericanExpress() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "American Express", "346710046000920", "12/19", "231");

        // Exercise
        boolean result = pp.verifyLuhn(card.cardNumber);

        // Verify
        assertTrue(result);
    }


    @Test
    public void testVerifyLuhnWrong() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282734", "12/19", "231");


        // Exercise
        boolean result = pp.verifyLuhn(card.cardNumber);

        // Verify
        assertFalse(result);
    }

    @Test
    public void testVerifyExpiredCard() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282734", "12/19", "231");


        // Exercise
        boolean result = pp.verifyExpiryDate(card.cardExpiryDate);

        // Verify
        assertTrue(result);
    }

    @Test
    public void testVerifyCorrectCard() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282734", "12/18", "231");


        // Exercise
        boolean result = pp.verifyExpiryDate(card.cardExpiryDate);

        // Verify
        assertFalse(result);
    }

    @Test
    public void testVerifyVisaCorrect16Digits() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");


        // Exercise
        int result = pp.verifyCardType(card.cardNumber, card.cardType);

        // Verify
        assertEquals(1, result);
    }

    @Test
    public void testVerifyMastercardCorrect() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Mastercard", "5137419418929912", "12/19", "231");


        // Exercise
        int result = pp.verifyCardType(card.cardNumber, card.cardType);

        // Verify
        assertEquals(1, result);
    }

    @Test
    public void testVerifyVisaIncorrect() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "5024650343282733", "12/19", "231");


        // Exercise
        int result = pp.verifyCardType(card.cardNumber, card.cardType);

        // Verify
        assertEquals(-1, result);
    }

    @Test
    public void testVerifyAllInformationIsGiven() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Mastercard", "5137419418929912", "12/19", "231");


        // Exercise
        boolean result = pp.verifyMissingInfo(card);

        // Verify
        assertFalse(result);
    }

    @Test
    public void testVerifyMissingInformationIsGiven() throws Exception {

        //Setup
        CCInfo card = new CCInfo("", "Dingli", "Mastercard", "5137419418929912", "12/19", "231");


        // Exercise
        boolean result = pp.verifyMissingInfo(card);

        // Verify
        assertTrue(result);
    }

    @Test
    public void testVerifyOfflineMissingInfo() throws Exception {
        //Setup
        CCInfo card = new CCInfo("", "Dingli", "Mastercard", "5137419418929912", "12/19", "231");


        // Exercise
        int result = pp.verifyOffline(card);

        // Verify
        assertEquals(-3, result);
    }

    @Test
    public void testVerifyOfflineAllConditionsMet() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");

        // Exercise
        int result = pp.verifyOffline(card);

        // Verify
        assertEquals(0, result);
    }

    @Test
    public void testVerifyOfflineExpiredCard() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/18", "231");

        // Exercise
        int result = pp.verifyOffline(card);

        // Verify
        assertEquals(-2, result);
    }

    @Test
    public void testVerifyOfflineIncorrectCardNumber() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282734", "12/19", "231");

        // Exercise
        int result = pp.verifyOffline(card);

        // Verify
        assertEquals(-1, result);
    }

    @Test
    public void testVerifyVisaCorrect13Digits() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4806699456584", "12/19", "231");


        // Exercise
        int result = pp.verifyCardType(card.cardNumber, card.cardType);

        // Verify
        assertEquals(1, result);
    }

    @Test
    public void testVerifyVisaIncorrect13Digits() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "mastercard", "4806699456584", "12/19", "231");


        // Exercise
        int result = pp.verifyCardType(card.cardNumber, card.cardType);

        // Verify
        assertEquals(-1, result);
    }

    @Test
    public void testVerifyAmericanExpressCorrect() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "American Express", "346710046000920", "12/19", "231");


        // Exercise
        int result = pp.verifyCardType(card.cardNumber, card.cardType);

        // Verify
        assertEquals(1, result);
    }

    @Test
    public void testVerifyAmericanExpressIncorrect() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "visa", "346710046000920", "12/19", "231");


        // Exercise
        int result = pp.verifyCardType(card.cardNumber, card.cardType);

        // Verify
        assertEquals(-1, result);
    }

    @Test
    public void testCorrectTransactionIsAuthorized() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.auth(card,1000)).thenReturn((long) 0);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(0, paymentProcessor.processAuth(card, 1000));
    }

    @Test
    public void testCreditCardDetailsInvalid() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282734", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.auth(card,1000)).thenReturn((long) -1);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-1, paymentProcessor.processAuth(card, 1000));
    }

    @Test
    public void testInsufficientFunds() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.auth(card,1000000)).thenReturn((long) -2);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-2, paymentProcessor.processAuth(card, 1000000));
    }

    @Test
    public void testUnknownErrorWhenAuthorised() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan1", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.auth(card,1000)).thenReturn((long) -3);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-3, paymentProcessor.processAuth(card, 1000));
    }

    @Test
    public void testCaptureTransaction() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.capture(1)).thenReturn(0);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(0, paymentProcessor.processCapture(1));
    }

    @Test
    public void testCaptureTransactionDoesNotExist() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.capture(14324)).thenReturn(-1);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-1, paymentProcessor.processCapture(14324));
    }

    @Test
    public void testCaptureTransactionAlreadyCaptured() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.capture(2)).thenReturn(-2);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-2, paymentProcessor.processCapture(2));
    }

    @Test
    public void testCaptureTransaction7DaysPassed() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.capture(10)).thenReturn(-3);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-3, paymentProcessor.processCapture(10));
    }

    @Test
    public void testCaptureTransactionUnkwonError() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "402465033282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.capture(23412341)).thenReturn(-4);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-4, paymentProcessor.processCapture(23412341));
    }

    @Test
    public void testRefundTransaction() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.refund((long) 1, (long) 150)).thenReturn(0);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(0, paymentProcessor.processRefund((long)1, (long) 150));
    }

    @Test
    public void testRefundTransactionDoesNotExist() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.refund((long) 1123, (long) 150)).thenReturn(-1);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-1, paymentProcessor.processRefund((long)1123, (long) 150));
    }

    @Test
    public void testRefundTransactionHasNotBeenCaptured() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.refund((long) 10, (long) 150)).thenReturn(-2);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-2, paymentProcessor.processRefund((long)10, (long) 150));
    }

    @Test
    public void testRefundTransactionHasBeenRefunded() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.refund((long) 2, (long) 100)).thenReturn(-3);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-3, paymentProcessor.processRefund((long)2, (long) 100));
    }

    @Test
    public void testRefundTransactionUnknownError() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "402465343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.refund((long) 123123123, (long) 1020)).thenReturn(-5);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-5, paymentProcessor.processRefund((long)123123123, (long) 1020));
    }

    @Test
    public void testRefundTransactionAmountIsBiggerThanCapturedAmount() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.refund((long) 5, (long) 1000000000)).thenReturn(-4);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(-4, paymentProcessor.processRefund((long)5, (long) 1000000000));
    }

    @Test
    public void testVerifyProcessPayment() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "231");

        // Exercise
        int result = pp.processPayment(card,1);

        // Verify
        assertEquals(0, result);
    }

    @Test
    public void testVerifyProcessPaymentUserGeneratedError() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282734", "12/19", "231");

        // Exercise
        int result = pp.processPayment(card,1);

        // Verify
        assertEquals(1, result);
    }

    @Test
    public void testVerifyProcessPaymentUnknownError() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "VISA", "4024650343282733", "12/19", "2311");
        BankProxy bankProxy = mock(BankProxy.class);
        when(bp.auth(card,1000)).thenReturn((long) 2);
        PaymentProcessor paymentProcessor = new PaymentProcessor(bp);

        // Verify
        assertEquals(2, paymentProcessor.processAuth(card, 1000));
    }

    @Test
    public void testNewTransactionDatabaseIsEmpty() throws Exception {
        assertEquals(0, db.totalTransactions());
    }


    @Test
    public void testAddNewTransaction() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282733", "12/19", "231");
        Transaction t = new Transaction(1,100,"1", card);

        // Exercise
        db.saveTransaction(t);

        // Verify
        assertEquals(1, db.totalTransactions());
    }

    @Test
    public void testAddThreeNewTransaction() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282733", "12/19", "231");

        // Exercise
        db.saveTransaction(new Transaction(1,100,"1", card));
        db.saveTransaction(new Transaction(2,1002,"1", card));
        db.saveTransaction(new Transaction(3,3100,"1", card));

        // Verify
        assertEquals(3, db.totalTransactions());
    }

    @Test
    public void testAddSameTransactionTwice() throws Exception {
        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282733", "12/19", "231");

        // Exercise
        db.saveTransaction(new Transaction(1,100,"1", card));
        db.saveTransaction(new Transaction(1,100,"1", card));

        // Verify
        assertEquals(1, db.totalTransactions());
    }

    @Test
    public void testCommitTransactionsSuccessfully() throws Exception {

        //Setup
        DBConnection dbConnection = new DBConnectionSuccessful();
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282733", "12/19", "231");

        // Exercise
        db.saveTransaction(new Transaction(1,100,"1", card));
        db.saveTransaction(new Transaction(2,1002,"1", card));
        db.saveTransaction(new Transaction(3,3100,"1", card));

        //Exercise
        boolean result = db.commit(dbConnection);

        //Verify
        assertTrue(result);
    }


    @Test(expected = Exception.class)
    public void testSaveNullTransaction() throws Exception {

        //Setup
        DBConnection dbConnection = new DBConnectionSuccessful();
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282733", "12/19", "231");
        Transaction transaction = null;
        db.saveTransaction(transaction);
    }

    @Test
    public void testCommitTransactionFails() throws Exception {

        //Setup
        DBConnection dbConnection = new DBConnectionFail();
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282733", "12/19", "231");


        //Exercise
        db.saveTransaction(new Transaction(1,100,"1", card));
        db.saveTransaction(new Transaction(2,1002,"1", card));
        db.saveTransaction(new Transaction(3,3100,"1", card));
        boolean result = db.commit(dbConnection);

        //Verify
        assertFalse(result);
    }

    @Test
    public void testCommitFailsOnSecondCommit() throws Exception {

        //Setup
        DBConnection dbConnection = new DBConnectionFailsOnSecondCommit();
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282733", "12/19", "231");

        //Exercise
        db.saveTransaction(new Transaction(1,100,"1", card));
        db.saveTransaction(new Transaction(2,1002,"1", card));
        db.saveTransaction(new Transaction(3,3100,"1", card));
        boolean result = db.commit(dbConnection);

        //Verify
        assertFalse(result);
    }

    @Test
    public void testDatabaseIsNotCommittedWhenNotDirty() throws Exception {

        //Setup
        DBConnectionSuccessfulSpy connection = new DBConnectionSuccessfulSpy();
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282733", "12/19", "231");
        db.saveTransaction(new Transaction(1,100,"1", card));
        db.saveTransaction(new Transaction(2,1002,"1", card));
        assertEquals(0, connection.getNumberOfTimesCalled());

        //Exercise
        db.commit(connection);
        db.commit(connection);
        db.commit(connection);

        //Verify
        assertEquals(db.totalTransactions(), connection.getNumberOfTimesCalled());

    }

    @Test
    public void testRemoveTransaction() throws Exception {

        //Setup
        CCInfo card = new CCInfo("Ryan", "Dingli", "Visa", "4024650343282733", "12/19", "231");

        // Exercise
        db.saveTransaction(new Transaction(1,100,"1", card));
        assertEquals(1,db.totalTransactions());

        // Verify
        db.removeTransaction(1);
        assertEquals(0, db.totalTransactions());
    }
}
