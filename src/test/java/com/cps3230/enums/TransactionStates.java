package com.cps3230.enums;

public enum TransactionStates {
    NOT_VERIFIED,
    VERIFIED,
    AUTHORISED,
    CAPTURED,
    VOID,
    REFUND
}
